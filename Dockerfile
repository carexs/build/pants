FROM ubuntu:20.04

RUN apt-get update && apt-get install wget curl apt-transport-https gnupg -y && \
	wget -qO - https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public | apt-key add - && \
	echo "deb https://adoptopenjdk.jfrog.io/adoptopenjdk/deb focal main" | tee /etc/apt/sources.list.d/adoptopenjdk.list && \
	apt update && \
	apt install adoptopenjdk-11-hotspot -y

RUN apt install -y python3.8 python3-pip python3-venv build-essential libssl-dev libffi-dev python3-dev

RUN apt install -y zip unzip

RUN apt install -y docker.io docker-compose

RUN rm -rf /var/lib/apt/lists/*

# Add a new user "gitlab-runner" with user id 1001
RUN useradd -m -u 1001 gitlab-runner

# user assumed to have same uid as user host, useful for local build-jobs inside container
RUN useradd -m -u 1000 local-user

# Change to non-root privilege
USER gitlab-runner

WORKDIR /opt/workspace
