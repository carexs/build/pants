# pants

pants is a docker image for pants tasks (we use it for our Gitlab runner).
The image is based on Ubuntu:20.04 and contains
- GraalVM CE jvm 11
- pants

default workspace is `/opt/workspace`
default user is `gitlab-runner` with uid `1001`
